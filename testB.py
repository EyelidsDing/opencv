import numpy as np
import cv2

#Create（VideoCapture）Obj
cap = cv2.VideoCapture('./20181211200700.webm')
#Define xvid - 编码器 For AVI Format)
# fourcc = cv2.VideoWriter_fourcc(*'XVID')
fourcc = cv2.VideoWriter_fourcc(*'MP42')
#Create（VideoWriter）Obj and Bind xvid - 编码器
out = cv2.VideoWriter('output.mp4', fourcc, 20.0, (640, 480))



if cap.isOpened() == False:
    print('loadvideo failed')
    cap.release()
    out.release()
    cv2.destroyAllWindows()

#threshold
threshold = 25
#read the fist frame
success, frame = cap.read()

while(success):
    frame = cv2.flip(frame, 0) #flip垂直反转，you can seem it as process part
    out.write(frame)
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    cv2.imshow('frame',gray)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
    success, frame = cap.read()

cap.release()
out.release()
gray.release()
cv2.destroyAllWindows()