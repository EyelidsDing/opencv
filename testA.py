import numpy as np
import cv2

#创建视频捕获（VideoCapture）对象
cap = cv2.VideoCapture('./20181211200700.webm')

#定义编解码器(xvid编码器对应avi格式)
fourcc = cv2.VideoWriter_fourcc(*'XVID')
#创建视频写入（VideoWriter）对象并绑定编码器
out = cv2.VideoWriter('output.avi', fourcc, 20.0, (640, 480))

threshold = 25
alpha = 0.01
if cap.isOpened()==False:
    print('loadvideo failed')
    cap.release()
    out.release()
    cv2.destroyAllWindows()

while(cap.isOpened()):
    ret, frame = cap.read()
    if ret == True:
        frame = cv2.flip(frame, 0) #这里也是处理每一个有效帧的代码
        out.write(frame)

    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    cv2.imshow('frame',gray)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
cap.release()
out.release()
cv2.destroyAllWindows()